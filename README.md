# Project Name

Exception exercise project. Use best practices to write Exception
    1. Always clean up after yourself
    2. Never use exceptions for flow control
    3. Do not suppress or ignore exceptions
    4. Do not catch top-level exceptions
    5. Log exceptions just once

## Installation

This project was created with Spring Tool Suite version 3.7.3. 
    1. Java 8
    2. maven 3

## Usage

### Stap 1 invoer validatie
In deze stap gaan we de methode importeerBestand in de FileImporterServiceImpl uitbreiden.  

Volg het fail fast principe en doe eerst een invoer validatie. De bestandsnaam mag niet leeg zijn en niet null. Gebruik hiervoor een bestaande Exceptie een IllegalArgumentException. Omdat de client/gebruiker van deze service methode een herstelactie zou kunnen uitvoeren o.b.v. deze invoer gaan we de Exceptie opnemen als throws in de methode declaratie.  

### Stap 2 Implementeer de printOutputMethode en Log de inhoud van het bestand als dummy implementatie van het importeren.

Maak de print output verder af en gebruik hiervoor de onderstaande code. Nu gaat het erom dat je een try, catch, finally goed gebruikt. Let erop dat je in een finally geen nieuwe exceptie gooit, vang deze af en log de output. Wanneer het bestand niet bestaat wordt er een FileNotFound Exceptie gegooid en deze wil je als gebruiker ook weten, dus gooi deze door tot aan de method declaratie van de importeerBestand methode.  

	BufferedReader br = new BufferedReader(new FileReader(importBestand));
	String line;
	while ((line = br.readLine()) != null) {
		LOGGER.debug(line);
	}
	br.close();
		

## History

2016-03-11 First checkin

## Credits

TODO: Write credits

## License
 
 Copyright Angarde Professionals in ICT 2016
 
## Used resources

[Best Practices for Exception Handling](http://www.onjava.com/pub/a/onjava/2003/11/19/exceptions.html?page=2)

[Designing with exceptions](http://www.javaworld.com/article/2076721/core-java/designing-with-exceptions.html)

[Ten practices for perfect Java Exception handling](http://literatejava.com/exceptions/ten-practices-for-perfect-java-exception-handling/)



 